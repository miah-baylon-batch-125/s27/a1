let http = require("http");
let port = 3000;

http.createServer( (request, response) => {
	if(request.url == "/login"){
		response.writeHead(200, 
			{"Content-Type": "text/html"});
		response.write("You are in the login page.")
		response.end()
	} else {
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
}).listen(port);

console.log("The serve is successfully running.");